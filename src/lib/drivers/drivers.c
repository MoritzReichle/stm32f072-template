/**
 *****************************************************************************
 * @title   drivers.c
 * @author  Daniel Schnell <dschnell@posteo.de>
 * @brief
 *****************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"



/* Function definitions */


void drivers_clocks_init()
{
    /* RCC_APB2Periph_SYSCFG, RCC_APB2Periph_SPI1 RCC_APB2Periph_USART1 RCC_APB2Periph_TIM17 Periph clock enable */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG |
    					   RCC_APB2Periph_SPI1 |
                           RCC_APB2Periph_USART1 |
                           RCC_APB2Periph_TIM17,
                           ENABLE);


    /* RCC_APB1Periph_SPI2, RCC_APB1Periph_PWR clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2 |
                           RCC_APB1Periph_PWR,
                           ENABLE);

    /* USART, SPI1, SPI2,  digital io's, ext io's, SPI request port */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA |
                          RCC_AHBPeriph_GPIOB |
                          RCC_AHBPeriph_GPIOC |
                          RCC_AHBPeriph_GPIOD |
                          RCC_AHBPeriph_GPIOF,
                          ENABLE);
}


/* EOF */

