/*
 * l3gd20.c
 *
 *  Created on: 18.11.2014
 *      Author: chris
 */

#include "l3gd20.h"
#include "spi.h"
#include <FreeRTOS.h>
#include "printf.h"

static void* spi_mems = 0;
/* Block for 500ms. */
static const TickType_t xDelay = 500 / portTICK_PERIOD_MS;



int l3gd20_init(void) {

	spi_mems = spi_open(2);
	if (NULL == spi_mems)
	{
		return 0;
	}
	return 1;
}

int l3gd20_read_register (uint8_t reg, uint8_t *data) {
	if(NULL == data) {
		return 0;
	}
	uint8_t cmd = (0x00|(1<<7 /*read bit*/))|reg;
	spi_write_partial(spi_mems, (void*)&cmd, 1,xDelay);
	spi_read(spi_mems,data,1,xDelay);
	return 1;
}

int l3gd20_write_register (uint8_t reg, uint8_t data) {

	uint16_t cmd = 0x0000 | reg;
	cmd <<= 8;        /* shift address to high byte */
	cmd |= data;      /* add data to write */
	const int size = sizeof(cmd);
	if( 1 != spi_write_partial(spi_mems, &reg,1,xDelay)) {
		return 0;
	} else if( 1 != spi_write(spi_mems, &data,1,xDelay)) {
		return 0;
	}
	return 1;
}

int l3gd20_readtemp(int8_t *temp) {
	return l3gd20_read_register(L3GD20_REGISTER_OUT_TEMP,(uint8_t*)temp);
}


int l3gd20_get_result(L3GD20Data_t *data) {
	uint8_t out = 0x00;
	int res = 0;
	if (NULL != data) {
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_X_L,&out);
		data->x_axis = out;
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_X_H,&out);
		data->x_axis |= out << 8;
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_Y_L,&out);
		data->y_axis = out;
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_Y_H,&out);
		data->y_axis |= out << 8;
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_Z_L,&out);
		data->z_axis |= out;
		res |= l3gd20_read_register(L3GD20_REGISTER_OUT_Z_H,&out);
		data->z_axis |= out << 8;
	}
	return res;
}


