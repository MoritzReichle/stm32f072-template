/**
 * @file
 *****************************************************************************
 * @title   diagctl.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief  diagctrl application
 *******************************************************************************/

#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* our own implementation of printf and sprintf */
#include "printf.h"

#include "stm32f0xx_syscfg.h"
#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_conf.h"
#include "stm32f0xx_usart.h"
#include "stm32f0xx_adc.h"
#include "stm32f0xx_rtc.h"

#include "usbd_cdc_core.h"
#include "usbd_usr.h"

#include <FreeRTOS.h>
#include <task.h>

#include "conversions.h"
#include "drivers.h"
#include "logger.h"
#include "os.h"
#include "usart.h"
#include "spi.h"
#include "xtimers.h"

#include "monitor.h"

#include "dbg_pin.h"

#include "worker_queue.h"

#include "l3gd20.h"
extern void hooks_get(void);


#define CFG_STACK_SIZE 150      // multiplied with 4 !
#define BINGO       do { } while(true)

/**
 * USB device handle for VCOM
 */
USB_CORE_HANDLE  USB_Device_dev;

/**
 * General purpose task that executes with priority 3
 */
static void worker_task(void* arg P_UNUSED)
{

	 /* Block for 500ms. */
	 const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	 L3GD20Data_t gyro;

	 GPIO_InitTypeDef  GPIO_InitStructure;

	/* Configure the GPIO_LED pin */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	uint8_t data = 0;
	int res = 0;
	int temp = -200;
	res = l3gd20_read_register(L3GD20_REGISTER_WHO_AM_I,&data);  /* check if we can communicate */
	res = l3gd20_read_register(L3GD20_REGISTER_CTRL_REG1,&data); /* read ctrl register 1 */
	data |= 1<<3;                                                /* enable device */
	res = l3gd20_write_register(L3GD20_REGISTER_CTRL_REG1,data);
	res = l3gd20_read_register(L3GD20_REGISTER_CTRL_REG1,&data); /* read ctrl register 1 */
	res = l3gd20_readtemp(&temp);


	log_msg("Worker Ready.");
    do
    {
    	GPIO_WriteBit(GPIOC, GPIO_Pin_6, Bit_RESET);
        vTaskDelay( xDelay );
        GPIO_WriteBit(GPIOC, GPIO_Pin_6, Bit_SET);
        vTaskDelay( xDelay );
        res = l3gd20_get_result(&gyro);

    	log_msg(".");
    } while (true);
}


/**
 * @brief  Main function of STM32F072 Demo software
 * @param  None
 * @retval None
 */
int main(void)
{
    // dummy function to resolve hook functions in libioc.a
    hooks_get();

    /* Enable clocks */
    drivers_clocks_init();

    /* Initializes peripherals */
    if (false == usart1_init())
    {
        BINGO;
    }
    /* Initialize debug pin facility */
    dbg_pin_init();

    /* spi communication */
    init_spi2();

    /*l3gd20 init*/
    l3gd20_init();
    printf("\r\n");
    printf("+===================+\r\n");
    printf("| STM32F072 Demo    |\r\n");
    printf("+===================+\r\n");
    printf("\r\n");
    printf("Press 'h' for help\r\n");

    /* logging is only available after scheduler is started */
    if (false == log_init())
    {
        BINGO;
    }

    /* initialize command monitor */
    if (false == monitor_init())
    {
        BINGO;
    }

    /* The Application layer has only to call USBD_Init to
      initialize the USB low level driver, the USB device library, the USB clock
      ,pins and interrupt service routine (BSP) to start the Library*/
      USBD_Init(&USB_Device_dev,
                &USR_desc,
                &USBD_CDC_cb,
                &USR_cb);

    xTaskHandle th = NULL;
    /* worker task */
    xTaskCreate(worker_task, (const char * const) "Worker", CFG_STACK_SIZE-25,
                (void *) 0, 2, &th);
    os_add_tcb(th);

    vTaskStartScheduler();

    // is only executed if task creation has not been successful
    BINGO;
}




