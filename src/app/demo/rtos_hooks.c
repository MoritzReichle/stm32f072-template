/**
 * @file
 *****************************************************************************
 * @title   hooks.c
 * @author  Daniel Schnell (deexschs)
 *
 * @brief  MRAM test functions
 *******************************************************************************/

#include <stdbool.h>
#include <FreeRTOS.h>           /*!< FreeRTOS header file */
#include <task.h>
#include <timers.h>

/* our own implementation of printf and sprintf */
#include "printf.h"
#include "os.h"


#define BINGO       do { } while(true)

static bool idleHookRanAlready = false;

void hooks_get(void)
{
    // dummy function to resolve all hook functions here that are part of lib<projectname>.a
    // and would otherwise not be linked against, because those are defined as weak symbols.
    // This function is called from main()
}

/**
 * Hook functions the RTOS links in
 */
void vApplicationMallocFailedHook(void)
{
    printf("Malloc Failed !\n");
    /* vApplicationMallocFailedHook() will only be called if
     configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
     function that will get called if a call to pvPortMalloc() fails.
     pvPortMalloc() is called internally by the kernel whenever a task, queue,
     timer or semaphore is created.  It is also called by various parts of the
     demo application.  If heap_1.c or heap_2.c are used, then the size of the
     heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
     FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
     to query the size of free heap space that remains (although it does not
     provide information on how the remaining heap might be fragmented). */

}

void vApplicationStackOverflowHook(xTaskHandle xTask,
        signed portCHAR* pcTaskName)
{
    (void) pcTaskName;
    (void) xTask;

    /* Run time stack overflow checking is performed if
     configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
     function is called if a stack overflow is detected. */taskDISABLE_INTERRUPTS();
    for (;;)
        ;
}

void vApplicationTickHook(void)
{
    /* This function will be called by each tick interrupt if
     configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
     added here, but the tick hook is called from an interrupt context, so
     code must not attempt to block, and only the interrupt safe FreeRTOS API
     functions can be used (those that end in FromISR()). */

}

/**
 * The idle hook is a task that is called if no other task is running.
 * This could e.g. be a task polling I/O's or sensors.
 *
 * It can provide very fast feedback because it runs almost all the time.
 */
void vApplicationIdleHook(void)
{
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
     to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
     task.  It is essential that code added to this hook function never attempts
     to block in any way (for example, call xQueueReceive() with a block time
     specified, or call vTaskDelay()).  If the application makes use of the
     vTaskDelete() API function then it is also
     important that vApplicationIdleHook() is permitted to return to its calling
     function, because it is the responsibility of the idle task to clean up
     memory allocated by the kernel to any task that has since been deleted. */

        /**
         * Things we might implement here:
         *
         * - Watchdog supervisor: we should regularly reset a watchdog trigger
         *   timer here. If the idle task is not scheduled often enough, we might
         *   be in trouble.
         *
         * - Flash Checks:
         *   Calculate CRC at startup and regularly run CRC check while running
         *   to see if we have a corrupted flash
         *
         * - idle task run calculations for load/performance/cpu idle time
         *   calculations in conjunction with the task switch hook. Idle
         *   task makes the calculations.
         *
         * - flushing any logging activities from the internal log buffer.
         */
    if (false == idleHookRanAlready)
    {
        os_add_tcb(xTaskGetIdleTaskHandle());
        os_add_tcb(xTimerGetTimerDaemonTaskHandle());
        idleHookRanAlready = true;
    }
}

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
    BINGO;
}


/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
    BINGO;
}


/**
  * @brief  This function handles WWDG_IRQHandler exception.
  * @param  None
  * @retval None
  */
void WWDG_IRQHandler()
{
    BINGO;
}

/* EOF */
